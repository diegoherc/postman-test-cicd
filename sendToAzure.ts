const fs = require("fs");
import * as azdev from "azure-devops-node-api";
import * as gitclient from "azure-devops-node-api/GitApi";

import {
  GitPush,
  GitCommitRef,
  GitChange,
  ItemContent,
  GitItem,
  GitRefUpdate,
} from "azure-devops-node-api/interfaces/GitInterfaces";
import { exit, report } from "process";

type IReport = {
  file: string;
};

function base64_encode(filePath: string) {
  // read binary data
  var fileText = fs.readFileSync(filePath);

  return fileText.toString("utf8");
}

export async function sendToAzure(
  reports: IReport[],
  refName: string,
  project: string,
  repostoryName: string,
  connection: azdev.WebApi
) {
  let git: gitclient.IGitApi = await connection.getGitApi();
  let repostories = await git.getRepositories(project);
  let gitrepo = repostories.find(
    (element: any) => element.name === repostoryName
  );
  let repostoryId = gitrepo?.id;
  let gitChanges: GitChange[] = [];

  reports.map((report) => {
    let content = base64_encode(report.file);
    gitChanges.push(<GitChange>{
      changeType: 2, //Edit = 2, Upload = 1 (Primeiro envio deve ser 1, depois 2)
      newContent: <ItemContent>{ content, contentType: 0 }, //0-> RawText = 0, Base64Encoded = 1,
      item: <GitItem>{
        path: report.file,
      },
    });
  });

  if (typeof repostoryId === "string") {
    let ref = (await git.getRefs(repostoryId, project)).find(
      (element) => element.name === refName
    );
    let refUpdates: GitRefUpdate[] = [
      <GitRefUpdate>{
        name: ref?.name,
        oldObjectId: ref?.objectId, //get ref->object id
      },
    ];
    let gitCommitRef: GitCommitRef[] = [
      <GitCommitRef>{
        changes: gitChanges,
        comment: "Add a file",
      },
    ];
    let gitPush: GitPush = <GitPush>{
      commits: gitCommitRef,
      refUpdates: refUpdates,
      repository: gitrepo,
    };
    console.log(repostoryId);
    return await git.createPush(gitPush, repostoryId, project);
  }
}
