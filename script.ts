import * as azdev from "azure-devops-node-api";
import { sendToAzure } from "./sendToAzure";

const DadosComum = {
  orgUrl: "https://dev.azure.com/diegoherculano",
  token: "o3xbxmgbgasinhldkolrm7ln32woije6jd3eo2cpmoimizlteg7q",
  project: "My first project",
  repostoryName: "project-cypress",
  refName: "refs/heads/main",
};

const reports = [
  {
    file: "testReport.html",
  },
  {
    file: "testReport.xml",
  },
];

let authHandler = azdev.getPersonalAccessTokenHandler(DadosComum.token);
let connection = new azdev.WebApi(DadosComum.orgUrl, authHandler);

sendToAzure(
  reports,
  DadosComum.refName,
  DadosComum.project,
  DadosComum.repostoryName,
  connection
);
