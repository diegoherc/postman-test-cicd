"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const azdev = __importStar(require("azure-devops-node-api"));
const sendToAzure_1 = require("./sendToAzure");
const DadosComum = {
    orgUrl: "https://dev.azure.com/diegoherculano",
    token: "o3xbxmgbgasinhldkolrm7ln32woije6jd3eo2cpmoimizlteg7q",
    project: "My first project",
    repostoryName: "project-cypress",
    refName: "refs/heads/main",
};
const reports = [
    {
        file: "testReport.html",
    },
    {
        file: "testReport.xml",
    },
];
let authHandler = azdev.getPersonalAccessTokenHandler(DadosComum.token);
let connection = new azdev.WebApi(DadosComum.orgUrl, authHandler);
(0, sendToAzure_1.sendToAzure)(reports, DadosComum.refName, DadosComum.project, DadosComum.repostoryName, connection);
