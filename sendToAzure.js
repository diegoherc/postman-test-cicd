"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sendToAzure = void 0;
const fs = require("fs");
function base64_encode(filePath) {
    // read binary data
    var fileText = fs.readFileSync(filePath);
    return fileText.toString("utf8");
}
async function sendToAzure(reports, refName, project, repostoryName, connection) {
    let git = await connection.getGitApi();
    let repostories = await git.getRepositories(project);
    let gitrepo = repostories.find((element) => element.name === repostoryName);
    let repostoryId = gitrepo === null || gitrepo === void 0 ? void 0 : gitrepo.id;
    let gitChanges = [];
    reports.map((report) => {
        console.log(report.file);
        let content = base64_encode(report.file);
        gitChanges.push({
            changeType: 2,
            newContent: { content, contentType: 0 },
            item: {
                path: report.file,
            },
        });
    });
    console.log(gitChanges);
    if (typeof repostoryId === "string") {
        let ref = (await git.getRefs(repostoryId, project)).find((element) => element.name === refName);
        let refUpdates = [
            {
                name: ref === null || ref === void 0 ? void 0 : ref.name,
                oldObjectId: ref === null || ref === void 0 ? void 0 : ref.objectId, //get ref->object id
            },
        ];
        let gitCommitRef = [
            {
                changes: gitChanges,
                comment: "Add a file",
            },
        ];
        let gitPush = {
            commits: gitCommitRef,
            refUpdates: refUpdates,
            repository: gitrepo,
        };
        console.log(repostoryId);
        return await git.createPush(gitPush, repostoryId, project);
    }
}
exports.sendToAzure = sendToAzure;
